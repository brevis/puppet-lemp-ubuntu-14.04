# create mysql db and user

$dbname      = "name"
$dbuser      = "user"
$dbpassword = "password"

mysql::grant { $dbname:
    mysql_user     => $dbuser,
    mysql_password => $dbpassword,
}