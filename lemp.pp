# lemp-ubuntu-14.04

## common utils
Exec {
    path => ["/usr/bin", "/bin", "/usr/sbin", "/sbin", "/usr/local/bin", "/usr/local/sbin"]
}

exec { 'apt_update':
    command => '/usr/bin/apt-get update'
}

package { ['acl', 'curl', 'git', 'htop', 'imagemagick', 'openssl', 'mailutils']:
    ensure => present,
    require => Exec['apt_update'],
}

## nginx
class { "nginx": }

$full_web_path = '/var/www'

define web::nginx_php_vhost (
  $backend_port         = 9000,
  $php                  = true,
  $proxy                = undef,
  $www_root             = "${full_web_path}/${name}/",
  $location_cfg_append  = undef,
) {
  nginx::resource::vhost { "${name}":
    ensure                => present,
    listen_port           => 80,
    www_root              => $www_root,
    owner                  => 'www-data',
    group                  => 'www-data',
    mode                   => '0755',
    access_log            => "/var/log/nginx/${name}_access.log",
    error_log             => "/var/log/nginx/${name}_error.log",
    location_cfg_append   => {
      try_files => '$uri $uri/ =404'
    },
    index_files           => [ 'index.php' ],
  }

  if $php {
    nginx::resource::location { "${name}_root":
      ensure          => present,
      vhost           => "${name}",
      www_root        => $www_root,
      location        => '~ \.php$',
      index_files     => ['index.php', 'index.html', 'index.htm'],
      proxy           => undef,
      fastcgi         => "unix:/var/run/php5-fpm.sock",
      fastcgi_script  => undef,
      location_cfg_append => {
        fastcgi_split_path_info => '^(.+\.php)(/.+)$',
        fastcgi_index => 'index.php',
        fastcgi_connect_timeout => '3m',
        fastcgi_read_timeout    => '3m',
        fastcgi_send_timeout    => '3m'
      }
    }
  }
}

web::nginx_php_vhost { "localhost":
    www_root => "/var/www/localhost",
    notify  => Service['nginx'],
}

file { ['/var/www', '/var/www/localhost']:
    ensure => directory,
    owner  => 'www-data',
    group  => 'www-data',
    mode   => '0755',
}

file { "/var/www/localhost/index.php":
    ensure  => file,
    content => '<?php phpinfo();',
}

file {['/etc/nginx/site-available/default', '/etc/nginx/site-enabled/default']:
    ensure  => absent,
}

## php-fpm
class { ['php::fpm', 'php::cli', 'php::extension::apc', 'php::extension::curl', 'php::extension::gd', 'php::extension::imagick', 'php::extension::mysql', 'php::extension::sqlite', 'php::extension::pgsql', 'php::extension::mcrypt', 'php::extension::intl']: }

exec { 'php_fix_pathinfo':
    command => 'sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" /etc/php5/fpm/php.ini',
    require => Package['php5-fpm'],
    notify  => Service['php5-fpm'],
}

exec { 'php_set_timezone_fpm':
    command => 'sed -i "s/;date.timezone =.*/date.timezone = Europe\/Moscow/" /etc/php5/fpm/php.ini',
    require => Package['php5-fpm'],
    notify  => Service['php5-fpm'],
}

exec { 'php_set_timezone_cli':
    command => 'sed -i "s/;date.timezone =.*/date.timezone = Europe\/Moscow/" /etc/php5/cli/php.ini',
    require => Package['php5-cli'],    
}

exec { 'php_fcgi_listen':
    command => 'sed -i "s/listen = 127.0.0.1:9000/listen = unix:\/var\/run\/php5-fpm.sock/" /etc/php5/fpm/pool.d/www.conf',
    require => Package['php5-fpm'],
    notify  => Service['php5-fpm'],
}

exec { 'php_allowed_clients':
    command => 'sed -i "s/;listen.allowed_clients = 127.0.0.1/listen.allowed_clients = 127.0.0.1/" /etc/php5/fpm/pool.d/www.conf',
    require => Package['php5-fpm'],
    notify  => Service['php5-fpm'],
}

## MySQL
class { 'mysql': 
    root_password => 'auto', # set a random root password (saved in /root/.my.cnf)
}

## Postfix & mailutils
class { 'postfix': }

# todo:
# create user, ssh keys
# set permissions/rights to www dir
# symfony host dummy;

