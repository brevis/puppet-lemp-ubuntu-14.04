Installation:
```
sudo apt-get update && sudo apt-get install -y curl git puppet
git clone https://bitbucket.org/brevis/puppet-lemp-ubuntu-14.04.git && cd puppet-lemp-ubuntu-14.04 && git submodule update --init --recursive
sudo puppet apply --modulepath=modules lemp.pp

# mysql db
sudo puppet apply --modulepath=modules mysqldb.pp
```